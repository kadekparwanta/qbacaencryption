﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QBacaEncryption
{
    public class EncHelper
    {
        private static byte[] MD5Hash(byte[] input)
        {
            try
            {
                return MD5Core.GetHash(input);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string ToUtf8Str(byte[] input)
        {
            try
            {
                var result = Encoding.UTF8.GetString(input, 0, input.Length);
                return result;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Decrypts the encrypted License from user_contents/check API to JSON string.
        /// </summary>
        /// <param name="DID">The Device ID byte.</param>
        /// <param name="data">The encrypted License byte.</param>
        /// <returns>A JSON string representation of the License.</returns>
        public static string DecryptLicense(byte[] DID, byte[] data)
        {
            try
            {
                byte[] commonKey = 
                { 
                    //Qbaca: 0x54, 0x65, 0x6c, 0x6b, 0x6f, 0x6d, 0x5f, 0x41, 0x6e, 0x64, 0x72, 0x6f, 0x69, 0x64, 0x21, 0x2a 
                    //Obook: 
                    0x6f,0x62,0x6f,0x6f,0x6b,0x21,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
                };

                //encryption key = (DID + common key)
                byte[] pass = new byte[DID.Length + commonKey.Length];
                Buffer.BlockCopy(DID, 0, pass, 0, DID.Length);
                Buffer.BlockCopy(commonKey, 0, pass, DID.Length, commonKey.Length);

                return Decrypt(pass, data);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Decrypts the encrypted file downloaded from License URL list.
        /// </summary>
        /// <param name="key">The key from License.</param>
        /// <param name="data">The encrypted data byte.</param>
        /// <returns>decrypted content, can be a html code (epub contents)</returns>
        public static String DecryptContent(string key, byte[] content)
        {
            try
            {
                byte[] newKey = Convert.FromBase64String(key);
                return Decrypt(newKey, content);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        private static String Decrypt(byte[] key, byte[] encryptedPlusSalt)
        {
            try
            {

                int num = 8;
                string str = "Salted__";
                int paddedSize = num + str.Length;

                //get salt
                byte[] salt = new byte[num];
                Buffer.BlockCopy(encryptedPlusSalt, num, salt, 0, num);

                //get encrypted data
                byte[] encrypted = new byte[encryptedPlusSalt.Length - paddedSize];
                Buffer.BlockCopy(encryptedPlusSalt, paddedSize, encrypted, 0, (encryptedPlusSalt.Length - paddedSize));

                //get key
                //encryption key (pass) = md5.hash(key + salt)
                byte[] pass = new byte[key.Length + num];
                Buffer.BlockCopy(key, 0, pass, 0, key.Length);
                Buffer.BlockCopy(salt, 0, pass, key.Length, num);
                byte[] hash = MD5Hash(pass);

                //RC4 decryption
                var resultBytes = RC4.Decrypt(hash, encrypted);
                var result = ToUtf8Str(resultBytes);
                return result;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }
    }
}
