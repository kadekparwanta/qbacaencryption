﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class RC4
{
    public static byte[] Decrypt(byte[] key, byte[] data)
    {
        try
        {
            return EncryptData(key, data);
        }
        catch (Exception)
        {
            return null;
        }
    }

    private static byte[] InitKey(byte[] key)
    {
        byte[] s = Enumerable.ToArray<byte>(from i in Enumerable.Range(0, 256) select (byte)i);
        int index = 0;
        int j = 0;
        while (index < 256)
        {
            j = ((j + key[index % key.Length]) + s[index]) & 0xff;
            Swap(s, index, j);
            index++;
        }
        return s;
    }

    private static byte[] EncryptData(byte[] key, byte[] data)
    {
        byte[] s = InitKey(key);
        int i = 0;
        int j = 0;
        List<byte> result = new List<byte>();
        foreach (var b in data)
        {
            i = (i + 1) & 0xff;
            j = (j + s[i]) & 0xff;
            Swap(s, i, j);
            result.Add((byte)(b ^ s[(s[i] + s[j]) & 0xff]));
        }
        return result.ToArray();
    }

    private static void Swap(byte[] s, int source, int dest)
    {
        byte num = s[source];
        s[source] = s[dest];
        s[dest] = num;
    }
}
